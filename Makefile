#makefile for MacOS
virtustan: virtustan.c virtustan.h world.h proolskript.c readw.c aux-skripts.c roomtypes.h
	gcc virtustan.c -o virtustan -DFREEBSD -DMACOS -I /usr/local/include /usr/lib/libiconv.dylib
	strip virtustan
clean:
	rm virtustan
